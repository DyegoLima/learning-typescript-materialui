export const listPrimary = [
  { name: 'Home', icon: 'home' },
  { name: 'Cadastro', icon: 'edit' },
  { name: 'Vaga', icon: 'comment' },
  { name: 'Suporte', icon: 'user' },
];

export const listSecondary = [
  { name: 'Configuração', icon: 'settings' },
  { name: 'Editor de Página', icon: 'window' },
  { name: 'Analytics', icon: 'trending' },
  { name: 'Documentos', icon: 'document' },
];
