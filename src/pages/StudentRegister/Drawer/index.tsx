import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import Icon from '../../../components/Icon';

import { listPrimary, listSecondary } from './listItems';

import {
  Container,
  DrawerStyled,
  ListItemText,
  List,
  ListSubheader,
  DivAppbarDrawer,
} from './styles';

export default function PermanentDrawerLeft() {
  return (
    <Container>
      <DrawerStyled variant="permanent" anchor="left">
        <DivAppbarDrawer />
        <ListSubheader>PAINEL</ListSubheader>
        <List>
          {listPrimary.map(list => (
            <ListItem button key={list.name}>
              <Icon name={list.icon} scale={16} />
              <ListItemText primary={list.name} />
            </ListItem>
          ))}
        </List>
        <Divider />
        <Divider />
        <Divider />
        <ListSubheader>CORPORATIVO</ListSubheader>
        <List>
          {listSecondary.map(list => (
            <ListItem button key={list.name}>
              <Icon name={list.icon} scale={16} />
              <ListItemText primary={list.name} />
            </ListItem>
          ))}
        </List>
      </DrawerStyled>
    </Container>
  );
}
