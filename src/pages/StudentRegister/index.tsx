import Layout from './Layout';

import AppBar from './AppBar';
import Drawer from './Drawer';
import Body from './Body';

const StudentRegister = () => {
  return (
    <div>
      <Layout AppBar={<AppBar />} Drawer={<Drawer />} Body={<Body />} />
    </div>
  );
};

export default StudentRegister;
