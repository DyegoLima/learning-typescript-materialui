import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
`;

export const Main = styled.main`
  flex-grow: 1;
  padding-top: 40px;
  padding-left: 24px;
  background-color: ${({ theme }) => theme.color.white};
  padding-top: 104px;
`;
