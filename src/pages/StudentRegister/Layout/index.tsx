import { ReactNode } from 'react';
import { Container, Main } from './styles';

interface LayoutProps {
  Drawer: ReactNode;
  AppBar: ReactNode;
  Body: ReactNode;
}

const Layout = ({ Drawer, AppBar, Body }: LayoutProps) => {
  return (
    <Container>
      {AppBar}
      {Drawer}
      <Main>{Body}</Main>
    </Container>
  );
};

export default Layout;
