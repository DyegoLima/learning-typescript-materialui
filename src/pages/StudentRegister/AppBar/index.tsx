import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { AppBarStyled, Container } from './styles';

const AppBar = () => {
  return (
    <Container>
      <AppBarStyled position="fixed">
        <Toolbar>
          <IconButton color="default">
            <Badge badgeContent={4} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
        </Toolbar>
      </AppBarStyled>
    </Container>
  );
};

export default AppBar;
