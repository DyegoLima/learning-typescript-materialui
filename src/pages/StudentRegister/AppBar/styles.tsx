import styled from 'styled-components';
import AppBarMaterialUi from '@material-ui/core/AppBar';

export const Container = styled.div``;

export const AppBarStyled = styled(AppBarMaterialUi)`
  height: 88px;
  justify-content: center;
  flex-direction: column;
  align-items: flex-end;

  &.MuiAppBar-colorPrimary {
    background-color: ${({ theme }) => theme.color.white};
    box-shadow: 0px 2px 4px rgba(40, 41, 61, 0.04),
      0px 8px 16px rgba(96, 97, 112, 0.16);
  }
`;
