import styled from 'styled-components';
import Drawer from '@material-ui/core/Drawer';
import ListItemTextUi from '@material-ui/core/ListItemText';
import ListMaterialUi from '@material-ui/core/List';
import ListSubheaderUi from '@material-ui/core/ListSubheader';
import Logo from '../../../assets/Logo.png';

const drawerWidth = '251px';

export const Container = styled.div`
  display: flex;
`;

export const DrawerStyled = styled(Drawer)`
  width: ${drawerWidth};
  flex-shrink: 0;
  .MuiDrawer-paper {
    width: ${drawerWidth};
    background: ${({ theme }) => theme.color.primary500};
  }
`;

export const DivAppbarDrawer = styled.div`
  background: url(${Logo}) no-repeat center;
  background-color: ${({ theme }) => theme.color.primary500};
  height: 88px;
  width: ${drawerWidth};
`;

export const ListSubheader = styled(ListSubheaderUi)`
  &.MuiListSubheader-gutters {
    padding: 0;
    width: 187px;
    padding-top: 16px;
    font-family: 'Roboto';
    font-weight: 500;
    font-style: normal;
    font-size: 12px;
    line-height: 21px;
    color: ${({ theme }) => theme.color.white};
  }
`;

export const List = styled(ListMaterialUi)`
  .MuiListItem-gutters {
    padding: 0;
    width: ${drawerWidth};
    height: 58px;
    padding-left: 29px;
    .MuiSvgIcon-root {
      color: ${({ theme }) => theme.color.white};
    }
  }
`;

export const ListItemText = styled(ListItemTextUi)`
  color: ${({ theme }) => theme.color.white};
  .MuiTypography-body1 {
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 24px;
    padding-left: 16px;
  }
`;
