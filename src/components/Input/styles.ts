import styled from 'styled-components';
import OutlinedInput from '@material-ui/core/TextField';

import { ContainerInputProps, SetConditinalColorsProps } from './interfaces';

export const Container = styled.div``;

const setConditionalColors = ({
  $success,
  error,
  theme,
}: SetConditinalColorsProps) => {
  if (error) return theme.color.danger500;
  return $success ? theme.color.success400 : theme.color.primary500;
};

export const StyledInput = styled(OutlinedInput)<ContainerInputProps>`
  input[type='number']::-webkit-inner-spin-button {
    -moz-appearance: textfield;
    appearance: textfield;
    -webkit-appearance: none;
  }
  input:-webkit-autofill {
    background-color: transparent;
    box-shadow: 0 0 0 1000px white inset;
    -webkit-box-shadow: ${({ theme }) =>
      `0 0 0 1000px ${theme.color.white} inset`};
  }

  //Outlined Config
  & .MuiOutlinedInput-root {
    width: 327px;
    height: 48px;
    color: ${setConditionalColors};
    background: ${({ theme }) => theme.color.white};

    & .MuiOutlinedInput-input {
      padding: 0;
    }

    :hover .MuiOutlinedInput-notchedOutline {
      border-color: ${setConditionalColors};
    }

    ::-webkit-outer-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }
    &.Mui-focused .MuiOutlinedInput-notchedOutline {
      border-color: ${setConditionalColors};
      border-width: 2px;
    }

    & .MuiOutlinedInput-notchedOutline {
      border-color: ${({ theme, $success }) =>
        $success ? theme.color.success400 : theme.color.black300};
    }

    &.Mui-disabled {
      border-color: ${({ theme }) => theme.color.black200};
      color: ${({ theme }) => theme.color.black200};
      background: ${({ theme }) => theme.color.black200};

      :hover .MuiOutlinedInput-notchedOutline {
        border-color: ${({ theme }) => theme.color.black200};
      }
      & .MuiOutlinedInput-notchedOutline {
        border-color: ${({ theme }) => theme.color.black200};
      }
    }
  }

  //Filled Config
  & .MuiFilledInput-root {
    width: 327px;
    height: 48px;
    color: ${setConditionalColors};
    background: ${({ theme }) => theme.color.white};

    :hover {
      background: ${({ theme }) => theme.color.white};
    }

    & .MuiFilledInput-input {
      padding: 0;
    }

    &.MuiFilledInput-underline:after {
      border-bottom: 2px solid ${setConditionalColors};
      transform: ${({ $success }) => ($success ? 'scaleX(1)' : '')};
    }

    &.Mui-disabled {
      border-color: ${({ theme }) => theme.color.black200};
      color: ${({ theme }) => theme.color.black200};
      background: ${({ theme }) => theme.color.black200};

      :hover .MuiOutlinedInput-notchedOutline {
        border-color: ${({ theme }) => theme.color.black200};
      }
      & .MuiOutlinedInput-notchedOutline {
        border-color: ${({ theme }) => theme.color.black200};
      }

      &.MuiFilledInput-underline.Mui-disabled:before {
        border-bottom-style: hidden;
      }
    }
  }

  // Base

  & .MuiInputBase-input {
    padding-left: 10px;
    color: ${({ theme }) => theme.color.black700};
    font-family: 'Roboto';
    font-weight: 400;
    font-style: normal;
    font-size: 14px;
    line-height: 24px;
    margin-left: 10px;
    :focus {
      outline: 1;
    }
  }

  &.MuiFormControl-root {
    align-items: self-end;
  }

  & .MuiFormHelperText-contained {
    margin-left: 2px;
    justify-content: center;
    align-items: center;
    display: flex;
  }

  & .MuiSvgIcon-root {
    color: ${({ error, theme }) =>
      error ? theme.color.danger500 : theme.color.success500};
    display: ${({ error, $success }) => (error || $success ? '' : 'none')};
  }
`;
