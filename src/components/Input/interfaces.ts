import { InputProps } from '@material-ui/core';
import { DefaultTheme } from 'styled-components';

export interface InputComponentProps extends InputProps {
  name: string;
  success?: boolean;
  icon?: 'left' | 'right' | undefined;
  variant: 'outlined' | 'filled';
}

export interface ContainerInputProps extends InputProps {
  name: string;
  $success?: string;
}

export interface SetConditinalColorsProps {
  $success?: string;
  error?: boolean | undefined;
  theme: DefaultTheme;
}
