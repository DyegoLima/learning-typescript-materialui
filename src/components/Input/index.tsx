import { useEffect, useRef, useState } from 'react';
import { useField } from '@unform/core';
import { Container, StyledInput } from './styles';

import MsgInputError from '../MsgInputError';
import Icon from '../Icon';

import { InputComponentProps } from './interfaces';

const Input = ({
  name,
  type,
  onBlur,
  onChange,
  disabled,
  icon,
  variant,
  placeholder,
}: InputComponentProps) => {
  const inputRef = useRef<HTMLInputElement>(null);

  const { fieldName, defaultValue, error, registerField } = useField(name);

  const [iconSuccessCondition, setIconSuccessCondition] = useState(false);

  useEffect(() => {
    setIconSuccessCondition(!!icon && !error && !!inputRef.current?.value);
  }, [setIconSuccessCondition, icon, error, inputRef.current?.value]);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
    });
  }, [fieldName, registerField]);

  return (
    <Container>
      <StyledInput
        disabled={disabled}
        $success={inputRef.current?.value}
        type={type}
        autoComplete="off"
        name={name}
        onBlur={onBlur}
        error={!!error}
        defaultValue={defaultValue}
        inputRef={inputRef}
        onChange={onChange}
        variant={variant}
        helperText={error ? <MsgInputError title={error} /> : ''}
        placeholder={placeholder}
        InputProps={{
          startAdornment: (
            <Icon
              name="success"
              scale={20}
              display={
                iconSuccessCondition && icon === 'right' ? undefined : 'none'
              }
            />
          ),
          endAdornment: (
            <Icon
              name="success"
              scale={20}
              display={
                iconSuccessCondition && icon === 'left' ? undefined : 'none'
              }
            />
          ),
        }}
      />
    </Container>
  );
};

export default Input;
