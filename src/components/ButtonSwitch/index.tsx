import { Container, SwitchStyled } from './styles';

import { ButtonSwitchProps } from './interfaces';

const ButtonSwitch = ({
  onChange,
  name,
  disabled,
  size,
  variant,
}: ButtonSwitchProps) => {
  return (
    <Container>
      <SwitchStyled
        name={name}
        variant={variant}
        onChange={onChange}
        disabled={disabled}
        size={size}
      />
    </Container>
  );
};

export default ButtonSwitch;
