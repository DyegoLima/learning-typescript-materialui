import ButtonSwitch from '../../components/ButtonSwitch';

 const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log(event.target.name, ' ', event.target.checked);
  };

  <ButtonSwitch
          name="Switch1"
          onChange={handleChange}
          variant="contained"
          size="medium"
        />
        <ButtonSwitch
          name="Switch2"
          onChange={handleChange}
          size="small"
          variant="contained"
        />
        <ButtonSwitch
          name="Switch3"
          onChange={handleChange}
          variant="outlined"
          size="medium"
        />
        <ButtonSwitch
          name="Switch4"
          onChange={handleChange}
          size="small"
          variant="outlined"
        />
