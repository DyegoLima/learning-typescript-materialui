import styled from 'styled-components';
import Switch from '@material-ui/core/Switch';

import { ButtonSwitchProps } from './interfaces';

const chooseSize = (size = '') => {
  switch (size) {
    case 'medium':
      return {
        thumb: '16px',
        width: '48px',
        height: '24px',
      };
    case 'small':
      return {
        width: '32px',
        height: '16px',

        thumb: '12px',
      };
    default:
      return {
        width: 0,
      };
  }
};

export const Container = styled.div``;

export const SwitchStyled = styled(Switch)<ButtonSwitchProps>`
  &.MuiSwitch-root {
    padding: ${({ variant, size }) =>
      variant === 'outlined' && size === 'medium' ? '8px' : ''};
  }

  & .MuiSwitch-switchBase {
    color: ${({ theme }) => theme.color.primary500};
  }

  & .MuiSwitch-colorSecondary.Mui-checked {
    color: #fff;
  }

  & .MuiSwitch-colorSecondary.Mui-checked + .MuiSwitch-track {
    background: ${({ theme }) => theme.color.primary500};
    opacity: 10;
  }

  &.MuiSwitch-sizeSmall .MuiSwitch-switchBase {
    padding: ${({ variant, size }) =>
      variant === 'outlined' && size === 'small' ? '0px' : '4px'};
    padding-top: ${({ variant, size }) =>
      variant === 'outlined' && size === 'small' ? '6px' : ''};
    padding-left: ${({ variant, size }) =>
      variant === 'outlined' && size === 'small' ? '8px' : ''};
  }

  &.MuiSwitch-sizeSmall .MuiSwitch-thumb {
    height: ${({ variant, size }) =>
      variant === 'outlined' ? chooseSize(size).thumb : '16px'};
    width: ${({ variant, size }) =>
      variant === 'outlined' ? chooseSize(size).thumb : '16px'};
  }

  &.MuiSwitch-sizeSmall .MuiSwitch-switchBase.Mui-checked {
    transform: ${({ variant, size }) =>
      variant === 'outlined' && size === 'small'
        ? 'translateX(12px)'
        : 'translateX(16px)'};
  }

  & .MuiSwitch-track {
    background-color: #fff;
    opacity: 10;
    border-radius: 15px;
    height: ${({ variant, size }) =>
      variant === 'outlined' ? chooseSize(size).height : '100%'};
    width: ${({ variant, size }) =>
      variant === 'outlined' ? chooseSize(size).width : '100%'};
    box-shadow: 0px 0px 1px rgba(40, 41, 61, 0.04),
      0px 2px 4px rgba(96, 97, 112, 0.16);
  }
`;
