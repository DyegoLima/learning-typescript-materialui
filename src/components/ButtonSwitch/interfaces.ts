import { SwitchProps } from '@material-ui/core/Switch';

export interface ButtonSwitchProps extends SwitchProps {
  name: string;
  variant: 'outlined' | 'contained';
}
