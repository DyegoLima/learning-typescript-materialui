export interface AvatarPropss {
  size: 'giant' | 'large' | 'medium' | 'small';
  badgeOn?: boolean;
  imageAvatar?: string;
}

export interface BadgeProps {
  size: 'giant' | 'large' | 'medium' | 'small';
  badgeColorOn?: boolean;
}
