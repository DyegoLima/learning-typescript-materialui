import styled from 'styled-components';
import AvatarMaterialUi from '@material-ui/core/Avatar';
import BadgeMaterialUi from '@material-ui/core/Badge';

import { AvatarPropss, BadgeProps } from './interfaces';

export const Container = styled.div``;

const chooseSize = (size = '') => {
  switch (size) {
    case 'giant':
      return {
        avatar: {
          width: '56px',
          height: '56px',
        },
        badge: {
          width: '14px',
          height: '14px',
        },
      };
    case 'large':
      return {
        avatar: {
          width: '48px',
          height: '48px',
        },
        badge: {
          width: '12px',
          height: '12px',
        },
      };
    case 'medium':
      return {
        avatar: {
          width: '32px',
          height: '32px',
        },
        badge: {
          width: '8px',
          height: '8px',
        },
      };
    case 'small':
      return {
        avatar: {
          width: '24px',
          height: '24px',
        },
        badge: {
          width: '6px',
          height: '6px',
        },
      };

    default:
      return {
        avatar: {
          width: 0,
          height: 0,
        },
        badge: {
          width: 0,
          height: 0,
        },
      };
  }
};

export const AvatarStyled = styled(AvatarMaterialUi)<AvatarPropss>`
  &.MuiAvatar-root {
    height: ${({ size }) => chooseSize(size).avatar.height};
    width: ${({ size }) => chooseSize(size).avatar.width};
    background: ${({ theme }) => theme.color.primary500};
  }
`;

export const Badge = styled(BadgeMaterialUi)<BadgeProps>`
  & .MuiBadge-badge {
    background: ${({ badgeColorOn, theme }) =>
      badgeColorOn ? theme.color.primary500 : theme.color.danger500};
    height: ${({ size }) => chooseSize(size).badge.height};
    min-width: ${({ size }) => chooseSize(size).badge.width};
    padding: 0;
    border: 2px;
    border-color: #fff;
    border-style: solid;
  }
`;
