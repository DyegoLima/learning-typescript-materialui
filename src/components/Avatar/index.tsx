import { Container, AvatarStyled, Badge } from './styles';
import { AvatarPropss } from './interfaces';

const Avatar = ({ size, badgeOn, imageAvatar }: AvatarPropss) => {
  return (
    <Container>
      {badgeOn ? (
        <Badge
          size={size}
          badgeColorOn={!!imageAvatar}
          badgeContent=""
          overlap="circle"
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
        >
          <AvatarStyled size={size} src={imageAvatar}>
            AB
          </AvatarStyled>
        </Badge>
      ) : (
        <AvatarStyled size={size} src={imageAvatar}>
          AB
        </AvatarStyled>
      )}
    </Container>
  );
};

export default Avatar;
