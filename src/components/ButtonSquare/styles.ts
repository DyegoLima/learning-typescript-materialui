import styled from 'styled-components';
import Button from '@material-ui/core/Button';

import { ButtonSquareComponentProps } from './interfaces';

const chooseSize = (size = '') => {
  switch (size) {
    case 'giant':
      return {
        width: '64px',
        height: '64px',
        fontSize: '30px',
        lineHeight: '37px',
      };
    case 'large':
      return {
        width: '48px',
        height: '48px',
        fontSize: '20px',
        lineHeight: '30px',
      };
    case 'medium':
      return {
        width: '40px',
        height: '40px',
        fontSize: '16px',
        lineHeight: '28px',
      };
    case 'small':
      return {
        width: '32px',
        height: '32px',
        fontSize: '14px',
        lineHeight: '24px',
      };
    default:
      return {
        width: 0,
      };
  }
};

export const Container = styled.div``;

export const ButtonStyled = styled(Button)<ButtonSquareComponentProps>`
  &.MuiButtonBase-root {
    font-weight: 700;
    font-family: 'Roboto';
    font-style: normal;

    width: ${({ scale }) => chooseSize(scale).width};
    height: ${({ scale }) => chooseSize(scale).height};
    font-size: ${({ scale }) => chooseSize(scale).fontSize};
    line-height: ${({ scale }) => chooseSize(scale).lineHeight};
    min-width: 0;
  }

  &.MuiButton-containedPrimary {
    background-color: ${({ theme }) => theme.color.primary500};
    &.MuiButton-containedPrimary:hover {
      background-color: ${({ theme }) => theme.color.primary600};
    }
  }

  &.MuiButton-outlinedPrimary {
    color: ${({ theme }) => theme.color.primary500};
    border: ${({ theme }) => `1px solid ${theme.color.primary500}`};
    &.MuiButton-outlinedPrimary:hover {
      border: ${({ theme }) => `1px solid ${theme.color.primary600}`};
    }
  }
`;
