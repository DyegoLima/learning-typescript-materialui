import { ButtonProps } from '@material-ui/core/Button';

export interface ButtonSquareComponentProps extends ButtonProps {
  title: string;
  icon?: string;
  variant: 'contained' | 'outlined';
  scale: 'giant' | 'large' | 'medium' | 'small';
}
