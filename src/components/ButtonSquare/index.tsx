import Icon from '../Icon';

import { Container, ButtonStyled } from './styles';
import { ButtonSquareComponentProps } from './interfaces';

const ButtonSquare = ({
  title,
  variant,
  scale,
  icon,
  ...props
}: ButtonSquareComponentProps) => {
  return (
    <Container>
      <ButtonStyled
        {...props}
        title={title}
        variant={variant}
        scale={scale}
        color="primary"
      >
        {icon && title ? (
          <Icon name="heart" scale={scale} />
        ) : (
          title.slice(0, 2)
        )}
      </ButtonStyled>
    </Container>
  );
};

export default ButtonSquare;
