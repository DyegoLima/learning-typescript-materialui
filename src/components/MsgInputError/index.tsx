import { Span } from './style';

import Icon from '../Icon';

const MsgInputError = ({ title = '' }) => {
  return (
    <>
      <Icon name="warning" scale={16} />
      <Span>{title}</Span>
    </>
  );
};

export default MsgInputError;
