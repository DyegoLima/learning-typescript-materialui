import styled from 'styled-components';

export const Span = styled.span`
  font-size: 12px;
  margin-left: 8px;
  margin-top: 4px;
  font-family: 'Roboto';
  font-weight: 400;
  font-style: normal;
  line-height: 21px;
`;
