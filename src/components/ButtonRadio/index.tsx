import { RadioGroupProps } from '@material-ui/core/RadioGroup';
import { Container, RadioGroup, Radio, FormControlLabel } from './styles';

import Icon from '../Icon';

import { RadioComponentPorps } from './interfaces';

const RadioButton = ({ children, onChange }: RadioGroupProps) => {
  return (
    <Container>
      <RadioGroup onChange={onChange}>{children}</RadioGroup>
    </Container>
  );
};

export const RadioItem = ({
  title,
  scale,
  variant,
  value,
  ...props
}: RadioComponentPorps) => {
  const renderRadio = (variantType: string) => {
    switch (variantType) {
      case 'roundCheked':
        return (
          <Radio
            value={value}
            scale={scale}
            variant={variant}
            {...props}
            color="primary"
            icon={<Icon name="circleOutline" scale="" />}
            checkedIcon={<Icon name="checkFill" scale="" />}
          />
        );
      case 'square':
        return (
          <Radio
            value={value}
            variant={variant}
            scale={scale}
            {...props}
            color="primary"
            icon={<Icon name="rounded" scale="" />}
            checkedIcon={<Icon name="roundedChecked" scale="" />}
          />
        );
      default:
        return (
          <Radio
            value={value}
            scale={scale}
            variant={variant}
            {...props}
            color="primary"
            icon={<Icon name="circleOutline" scale="" />}
          />
        );
    }
  };

  return (
    <FormControlLabel
      scale={scale}
      value={value}
      control={renderRadio(variant)}
      label={title}
    />
  );
};

export default RadioButton;
