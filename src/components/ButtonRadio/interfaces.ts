import { RadioProps } from '@material-ui/core/Radio';
import { FormControlLabelProps } from '@material-ui/core';

export interface RadioComponentPorps extends RadioProps {
  variant: 'square' | 'round' | 'roundCheked';
  value: string;
  title?: string;
  scale: 'giant' | 'medium' | 'small';
}

export interface FormControlComponentProps extends FormControlLabelProps {
  scale: 'giant' | 'medium' | 'small';
}
