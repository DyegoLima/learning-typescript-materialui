import styled from 'styled-components';
import RadioMaterialUi from '@material-ui/core/Radio';
import RadioGroupMaterialUi from '@material-ui/core/RadioGroup';
import FormControlLabelUi from '@material-ui/core/FormControlLabel';

import { RadioComponentPorps, FormControlComponentProps } from './interfaces';

const chooseSize = (size = '') => {
  switch (size) {
    case 'giant':
      return {
        width: '24px',
        height: '24px',
        fontSize: '20px',
        lineHeight: '30px',
      };
    case 'medium':
      return {
        width: '20px',
        height: '20px',
        fontSize: '16px',
        lineHeight: '28px',
      };
    case 'small':
      return {
        width: '16px',
        height: '16px',
        fontSize: '14px',
        lineHeight: '24px',
      };
    default:
      return {
        width: 0,
      };
  }
};

export const Container = styled.div``;

export const RadioGroup = styled(RadioGroupMaterialUi)`
  &.MuiFormGroup-root {
    align-items: baseline;
  }
`;

export const FormControlLabel = styled(
  FormControlLabelUi,
)<FormControlComponentProps>`
  & .MuiTypography-root {
    font-size: ${({ scale }) => chooseSize(scale).fontSize};
    line-height: ${({ scale }) => chooseSize(scale).lineHeight};
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
  }
`;

export const Radio = styled(RadioMaterialUi)<RadioComponentPorps>`
  & .MuiSvgIcon-root {
    min-width: ${({ scale }) => chooseSize(scale).width};
    min-height: ${({ scale }) => chooseSize(scale).height};
    width: ${({ scale }) => chooseSize(scale).width};
    height: ${({ scale }) => chooseSize(scale).height};
  }
`;
