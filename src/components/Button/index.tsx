import { Container, ButtonStyled } from './styles';
import Icon from '../Icon';

import { ButtonComponentProps } from './interfaces';

const Button = ({
  title,
  variant,
  disabled = false,
  scale,
  startIcon,
  endIcon,
  between,
  ...props
}: ButtonComponentProps) => {
  return (
    <Container>
      <ButtonStyled
        {...props}
        variant={variant}
        color="primary"
        scale={scale}
        disabled={disabled}
        title={title}
        endIcon={
          endIcon ? <Icon name={String(endIcon)} scale={scale} /> : undefined
        }
        startIcon={
          startIcon ? (
            <Icon name={String(startIcon)} scale={scale} />
          ) : undefined
        }
        between={startIcon || endIcon ? between : undefined}
        {...props}
      >
        {title}
      </ButtonStyled>
    </Container>
  );
};

export default Button;
