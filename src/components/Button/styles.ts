import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import { ButtonComponentProps } from './interfaces';

const chooseSize = (size = '') => {
  switch (size) {
    case 'giant':
      return {
        height: '64px',
        fontSize: '23px',
        lineHeight: '32',
        padding: '16px 72px',
      };
    case 'large':
      return {
        height: '48px',
        fontSize: '18px',
        lineHeight: '32',
        padding: '8px 47px',
      };
    case 'medium':
      return {
        height: '40px',
        fontSize: '14px',
        lineHeight: '24',
        padding: '8px 35px',
      };
    case 'small':
      return {
        height: '32px',
        fontSize: '12px',
        lineHeight: '16',
        padding: '8px 29px',
      };
    default:
      return {
        width: 0,
      };
  }
};

export const Container = styled.div``;

export const ButtonStyled = styled(Button)<ButtonComponentProps>`
  &.MuiButtonBase-root {
    height: ${({ scale }) => chooseSize(scale).height};
    font-size: ${({ scale }) => chooseSize(scale).fontSize};
    line-height: ${({ scale }) => chooseSize(scale).lineHeight};
    text-transform: none;
    font-weight: 500;
    font-family: 'Roboto';
    font-style: normal;
    justify-content: ${({ between }) => (between ? 'space-between' : '')};
  }

  &.MuiButton-containedPrimary {
    background-color: ${({ theme }) => theme.color.primary500};
    &.MuiButton-containedPrimary:hover {
      background-color: ${({ theme }) => theme.color.primary600};
    }
  }

  & .MuiButton-label {
    padding: ${({ scale }) => chooseSize(scale).padding};
  }

  &.MuiButton-outlinedPrimary {
    color: ${({ theme }) => theme.color.primary500};
    border: ${({ theme }) => `1px solid ${theme.color.primary500}`};
    &.MuiButton-outlinedPrimary:hover {
      border: ${({ theme }) => `1px solid ${theme.color.primary600}`};
    }
  }
`;
