import { ButtonProps } from '@material-ui/core/Button';

export interface ButtonComponentProps extends ButtonProps {
  variant: 'outlined' | 'contained';
  title: string;
  scale: 'giant' | 'large' | 'medium' | 'small';
  endIcon?: string | JSX.Element | undefined;
  startIcon?: string | JSX.Element | undefined;
  between?: 'true';
}
