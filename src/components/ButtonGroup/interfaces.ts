import { ReactNode } from 'react';

export interface ButtonGroupProps {
  children: ReactNode;
}

export interface ButtonItemProps {
  title?: string;
  iconName?: string;
  onClick(event: React.SyntheticEvent<HTMLButtonElement>): void;
}
