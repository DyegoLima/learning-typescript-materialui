import { Container, ButtonGroupStyled, ButtonItemStyled } from './styles';

import { ButtonItemProps, ButtonGroupProps } from './interfaces';

import Icon from '../Icon';

const ButtonGroup = ({ children }: ButtonGroupProps) => {
  return (
    <Container>
      <ButtonGroupStyled fullWidth>{children}</ButtonGroupStyled>
    </Container>
  );
};

export const ButtonItem = ({ title, iconName, onClick }: ButtonItemProps) => {
  return (
    <ButtonItemStyled variant="contained" iconName={iconName} onClick={onClick}>
      {iconName ? <Icon name={iconName} scale={16} /> : title}
    </ButtonItemStyled>
  );
};

export default ButtonGroup;
