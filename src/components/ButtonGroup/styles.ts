import styled from 'styled-components';
import ButtonGroupMaterialUi from '@material-ui/core/ButtonGroup';
import ButtonMaterialUi from '@material-ui/core/Button';

import { ButtonItemProps } from './interfaces';

export const Container = styled.div``;

export const ButtonGroupStyled = styled(ButtonGroupMaterialUi)`
  & .MuiButton-contained {
    font-family: 'Roboto';
    font-weight: 500;
    font-style: normal;
    font-size: 14px;
    line-height: 24px;
    text-transform: none;

    width: 328px;
    height: 40px;

    background: #ffffff;
    border-radius: 0px;

    padding: 0px;
    box-shadow: 0px 0px 1px rgba(40, 41, 61, 0.04),
      0px 2px 4px rgba(96, 97, 112, 0.16);
  }

  & .MuiButton-contained:hover {
    background-color: ${({ theme }) => theme.color.primary500};
    & .MuiButton-label {
      color: #ffffff;
      stroke: ${({ theme }) => theme.color.white};
    }
  }
  & .MuiButton-label {
    color: ${({ theme }) => theme.color.primary500};
    stroke: #ffffff;
    stroke: ${({ theme }) => theme.color.black700};
  }
`;

export const ButtonItemStyled = styled(ButtonMaterialUi)<ButtonItemProps>`
  & .MuiButton-label {
    color: ${({ iconName }) => (iconName ? 'white' : 'blue')};
  }
`;
