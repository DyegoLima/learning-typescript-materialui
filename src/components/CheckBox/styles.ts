import styled from 'styled-components';
import Checkbox from '@material-ui/core/Checkbox';
import FormLabelMaterialUi from '@material-ui/core/FormControlLabel';

import { CheckBoxComponentProps, FormControProps } from './interfaces';

const chooseSize = (size = '') => {
  switch (size) {
    case 'giant':
      return {
        width: '24px',
        height: '24px',
        fontSize: '20px',
        lineHeight: '30px',
      };
    case 'large':
      return {
        width: '20px',
        height: '20px',
        fontSize: '16px',
        lineHeight: '28px',
      };
    case 'medium':
      return {
        width: '16px',
        height: '16px',
        fontSize: '14px',
        lineHeight: '24px',
      };
    default:
      return {
        width: 0,
      };
  }
};

export const Container = styled.div``;

export const CheckBoxStyled = styled(Checkbox)<CheckBoxComponentProps>`
  padding-left: 8px;
  &.MuiCheckbox-root {
    color: ${({ theme }) => theme.color.black600};
  }

  & .MuiSvgIcon-root {
    min-width: ${({ scale }) => chooseSize(scale).width};
    min-height: ${({ scale }) => chooseSize(scale).height};
    width: ${({ scale }) => chooseSize(scale).width};
    height: ${({ scale }) => chooseSize(scale).height};
  }
  &.MuiCheckbox-colorPrimary.Mui-checked {
    color: ${({ theme }) => theme.color.primary500};
  }
`;

export const FormControlLabel = styled(FormLabelMaterialUi)<FormControProps>`
  & .MuiTypography-body1 {
    color: ${({ theme }) => theme.color.black700};
    font-family: 'Roboto';
    font-style: normal;
    font-weight: normal;
    font-size: ${({ scale }) => chooseSize(scale).fontSize};
    line-height: ${({ scale }) => chooseSize(scale).lineHeight};
  }
`;
