import { CheckboxProps } from '@material-ui/core/Checkbox';
import { FormControlLabelProps } from '@material-ui/core/FormControlLabel';

export interface CheckBoxComponentProps extends CheckboxProps {
  name: string;
  rounded?: boolean;
  label?: string;
  scale: 'giant' | 'large' | 'medium';
  disabled?: boolean;
}

export interface FormControProps extends FormControlLabelProps {
  scale: 'giant' | 'large' | 'medium';
}
