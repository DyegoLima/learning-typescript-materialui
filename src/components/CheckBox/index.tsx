import { Container, CheckBoxStyled, FormControlLabel } from './styles';
import Icon from '../Icon';

import { CheckBoxComponentProps } from './interfaces';

const CheckBox = ({
  scale,
  rounded = false,
  disabled,
  name,
  label,
  ...props
}: CheckBoxComponentProps) => {
  return (
    <Container>
      <FormControlLabel
        scale={scale}
        control={
          rounded ? (
            <CheckBoxStyled
              disabled={disabled}
              scale={scale}
              color="primary"
              icon={<Icon name="circleOutline" scale="" />}
              checkedIcon={<Icon name="checkFill" scale="" />}
              name={name}
              {...props}
            />
          ) : (
            <CheckBoxStyled
              disabled={disabled}
              color="primary"
              scale={scale}
              name={name}
              {...props}
            />
          )
        }
        label={disabled && label !== undefined ? 'Desativada' : label}
      />
    </Container>
  );
};

export default CheckBox;
