import { SvgIcon } from './styles';
import { icon } from '../../utils/icons';

import { IconProps } from './interfaces';

const Icon = ({ name, scale, ...props }: IconProps) => {
  return (
    <SvgIcon {...props} scale={scale}>
      <path d={icon[`${name}`]} />
    </SvgIcon>
  );
};

export default Icon;
