import { SvgIcon as SvgIconMaterialUi } from '@material-ui/core';

import styled from 'styled-components';

const chooseSize = (size = '') => {
  switch (size) {
    case 'giant':
      return {
        width: '28px',
        height: '24px',
      };
    case 'large':
      return {
        width: '22px',
        height: '19.25px',
      };
    case 'medium':
      return {
        width: '16px',
        height: '14px',
      };
    case 'small':
      return {
        width: '14px',
        height: '12.25px',
      };
    default:
      return {
        width: 0,
      };
  }
};

export const SvgIcon = styled(SvgIconMaterialUi)`
  &.MuiSvgIcon-root {
    display: ${({ display }) => (display ? 'none' : '')};
    width: ${({ scale }) =>
      typeof scale === 'number' ? `${scale}px` : chooseSize(scale).width};
    height: ${({ scale }) =>
      typeof scale === 'number' ? `${scale}px` : chooseSize(scale).height};
  }
`;
