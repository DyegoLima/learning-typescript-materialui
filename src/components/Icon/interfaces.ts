export interface IconProps {
  scale: string | number;
  name: string;
  display?: 'none';
}
