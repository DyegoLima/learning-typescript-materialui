import { ButtonComponentProps } from './interfaces';
import { Container, Button } from './styles';

import Icon from '../Icon';

const ButtonLink = ({
  title,
  scale,
  iconPosition,
  src,
}: ButtonComponentProps) => {
  const chooseSize = (size = '') => {
    switch (size) {
      case 'large':
        return 20;
      case 'medium':
        return 18;
      case 'small':
        return 16;
      default:
        return 0;
    }
  };

  const handleEndIcon = () => {
    return iconPosition === 'left' ? (
      <Icon name="arrowRightAlt" scale={chooseSize(scale)} />
    ) : (
      ''
    );
  };

  const handleStartIcon = () => {
    return iconPosition === 'right' ? (
      <Icon name="arrowRightAlt" scale={chooseSize(scale)} />
    ) : (
      ''
    );
  };

  return (
    <Container>
      <Button
        startIcon={handleStartIcon()}
        endIcon={handleEndIcon()}
        scale={scale}
        href={src}
      >
        {title}
      </Button>
    </Container>
  );
};

export default ButtonLink;
