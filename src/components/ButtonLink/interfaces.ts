import { ButtonHTMLAttributes } from 'react';

export interface ButtonComponentProps
  extends ButtonHTMLAttributes<HTMLButtonElement> {
  title: string;
  scale: 'large' | 'medium' | 'small';
  iconPosition: 'right' | 'left';
  src: string;
}

export interface LinkButtonProps {
  scale: string;
}
