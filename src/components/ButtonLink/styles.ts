import styled from 'styled-components';
import ButtonMaterialUi from '@material-ui/core/Button';
import LinkMaterialUi from '@material-ui/core/Link';

import { LinkButtonProps } from './interfaces';

const chooseSize = (size = '') => {
  switch (size) {
    case 'large':
      return {
        width: '54px',
        height: '32px',
        fontFamily: 'SF Pro Display',
        fontWeight: '600',
        fontSize: '18px',
        lineHeight: '32px',
      };
    case 'medium':
      return {
        width: '44px',
        height: '24px',
        fontFamily: 'Roboto',
        fontWeight: '500',
        fontSize: '14px',
        lineHeight: '24px',
      };
    case 'small':
      return {
        width: '38px',
        height: '24px',
        fontFamily: 'Roboto',
        fontWeight: '500',
        fontSize: '10px',
        lineHeight: '16px',
      };
    default:
      return {
        width: 0,
      };
  }
};

export const Container = styled.div``;

export const Link = styled(LinkMaterialUi)``;

export const Button = styled(ButtonMaterialUi)<LinkButtonProps>`
  &.MuiButton-root {
    &.MuiButton-root:hover {
      text-decoration: none;
      background-color: rgba(0, 0, 0, 0);
    }
  }
  &.MuiButtonBase-root {
    width: ${({ scale }) => chooseSize(scale).width};
    height: ${({ scale }) => chooseSize(scale).height};
    font-family: ${({ scale }) => chooseSize(scale).fontFamily};
    font-weight: ${({ scale }) => chooseSize(scale).fontWeight};
    font-size: ${({ scale }) => chooseSize(scale).fontSize};
    line-height: ${({ scale }) => chooseSize(scale).lineHeight};
    font-style: normal;
    min-width: 0;
    color: ${({ theme }) => theme.color.primary500};
    text-transform: none;
  }
  & .MuiButton-startIcon {
    margin-right: 0;
  }
  & .MuiButton-endIcon {
    margin-left: 0;
  }
`;
