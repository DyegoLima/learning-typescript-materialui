/* eslint-disable @typescript-eslint/no-explicit-any */
export interface FileProps {
  file: any;
  name: any;
  id: string;
  readableSize: string;
  preview: string;
  uploaded: boolean;
  error: boolean;
  url: null;
}

export interface UploadProps {
  iconName: string;
  iconScale: string | number;
}
