import styled from 'styled-components';
import IconButtonMaterial from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PhotoCamera';

interface dropzoneProps {
  isDragActive: boolean;
  isDragReject: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  uploadImage: any;
}

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 160px;
  height: 160px;
  border-width: 2px;
  border-radius: 50%;
  border-color: #c4c4c4;
  border-style: solid;
`;

export const IconButton = styled(IconButtonMaterial)<dropzoneProps>`
  &.MuiButtonBase-root {
    color: #959393;
    width: 150px;
    height: 150px;
    border-radius: 50%;
    background: #eaeaea;
    border-width: 1px;
    border-color: #959393;
    background-image: url(${props => props.uploadImage});
    background-position: 50% 50%;
    background-size: cover;
  }
`;

export const Icons = styled(PhotoCamera)`
  &.MuiSvgIcon-root {
    width: 44.8px;
    height: 38.4px;
    color: #959393;
  }
`;
