/* eslint-disable @typescript-eslint/no-explicit-any */
import { useState } from 'react';
import Dropzone from 'react-dropzone';

import { uniqueId } from 'lodash';
import fileSize from 'filesize';

import { Container, IconButton } from './styles';
import Icon from '../Icon';

import { FileProps, UploadProps } from './interfaces';

const ButtonUpload = ({ iconName, iconScale = 84.8 }: UploadProps) => {
  const [uploadImage, setUploadImage] = useState<FileProps[]>([]);

  const handleUpload = (files: Array<any>) => {
    const uploadFile = files.map(item => ({
      file: item,
      name: item.name,
      id: uniqueId(),
      readableSize: fileSize(item.size),
      preview: URL.createObjectURL(item),
      uploaded: false,
      error: false,
      url: null,
    }));

    setUploadImage(uploadFile);
  };
  return (
    <Container>
      <Dropzone accept="image/*" onDropAccepted={e => handleUpload(e)}>
        {({ getRootProps, getInputProps, isDragActive, isDragReject }) => (
          <IconButton
            uploadImage={uploadImage.length !== 0 && uploadImage[0].preview}
            {...getRootProps()}
            color="primary"
            isDragActive={isDragActive}
            isDragReject={isDragReject}
            aria-label="upload picture"
          >
            {uploadImage.length !== 1 && (
              <Icon name={iconName} scale={iconScale} />
            )}
            <input type="file" {...getInputProps()} />
          </IconButton>
        )}
      </Dropzone>
    </Container>
  );
};

export default ButtonUpload;
