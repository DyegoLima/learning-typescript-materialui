import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    title: stirng;

    color: {
      black900: stirng;
      black800: stirng;
      black700: stirng; // (Base)
      black600: stirng;
      black500: stirng;
      black400: stirng;
      black300: stirng;
      black200: stirng;
      black100: stirng;

      primary700: string;
      primary600: string;
      primary500: string; // (Base)
      primary400: string;
      primary300: string;
      primary200: string;
      primary100: string;

      danger700: string;
      danger600: string;
      danger500: string; // (Base)
      danger400: string;
      danger300: string;
      danger200: string;

      success700: string;
      success600: string;
      success500: string; // (Base)
      success400: string;
      success300: string;
      success200: string;

      warning700: string;
      warning600: string;
      warning500: string; // (Base)
      warning400: string;
      warning300: string;
      warning200: string;

      white: string;
      neutralBlue: string;
      lightBlue: string;
      border: string;
      BG: string;
      drag: string;

      gradientButtonBlack: string;
      gradientButtonPrimary: string;
      gradientButtonDanger: string;
      gradientButtonWarning: string;
      gradientButtonSuccess: string;

      gradientFullBlack: string;
      gradientFullPrimary: string;
      gradientFullDanger: string;
      gradientFullWarning: string;
      gradientFullSuccess: string;
      gradientFullPrimaryDanger: string;
      gradientFullPrimaryWarning: string;
      gradientFullPrimarySuccess: string;

      brandColorsPrimary: string;
      brandColors01: string;
      brandColors02: string;
      brandColors03: string;
      brandColors04: string;
      brandColors05: string;
      brandColors06: string;
      brandColors07: string;
      brandColors08: string;
      brandColors09: string;
      brandColors10: string;
    };
  }
}
