export default {
  title: 'light',
  color: {
    black900: '#121212',
    black800: '#212121',
    black700: '#3F3B3B', // (Base)
    black600: '#605F5F',
    black500: '#959393',
    black400: '#C4C4C4',
    black300: '#EAEAEA',
    black200: '#F5F5F5',
    black100: '#FAFAFB',

    primary700: '#01459D',
    primary600: '#085ED0',
    primary500: '#006EFF', // (Base)
    primary400: '#59A0FF',
    primary300: '#97C3FF',
    primary200: '#EBF4FF',
    primary100: '#F6FAFF',

    danger700: '#9C3E3E',
    danger600: '#C54E4E',
    danger500: '#F46363', // (Base)
    danger400: '#F67878',
    danger300: '#F89595',
    danger200: '#FFCCCC',

    success700: '#1F9050',
    success600: '#27B264',
    success500: '#2ED477', // (Base)
    success400: '#5FDD97',
    success300: '#9CFFC8',
    success200: '#C6FFDF',

    warning700: '#B86927',
    warning600: '#DC7D30',
    warning500: '#FF9138', // (Base)
    warning400: '#FFA65E',
    warning300: '#FEBB83',
    warning200: '#FFD5B3',

    white: '#FFFFFF',
    neutralBlue: '#5D6F88',
    lightBlue: '#ADD0FF',
    border: '#B9B9B9',
    BG: '#F2F2F2',
    drag: '#D8D8D8',

    gradientButtonBlack:
      'linear-gradient(180deg, rgba(18, 18, 18, 0.0001) 0%, #121212 100%)',
    gradientButtonPrimary:
      'linear-gradient(180deg, rgba(0, 110, 255, 0.0001) 0%, #006EFF 100%)',
    gradientButtonDanger:
      'linear-gradient(180deg, rgba(244, 99, 99, 0.0001) 0.25%, #F46363 100%)',
    gradientButtonWarning:
      'linear-gradient(180deg, rgba(255, 145, 56, 0.0001) 0.37%, #FF9138 100%)',
    gradientButtonSuccess:
      'linear-gradient(180deg, rgba(129, 255, 184, 0.0001) 0%, #2ED477 100%)',

    gradientFullBlack: 'linear-gradient(180deg, #121212 0.11%, #757575 100%)',
    gradientFullPrimary:
      'linear-gradient(180deg, #4B99FF 31.75%, #80B7FF 100%)',
    gradientFullDanger: 'linear-gradient(180deg, #F46363 0%, #FF9D9D 100%)',
    gradientFullWarning:
      'linear-gradient(180deg, #FF9138 31.75%, rgba(255, 145, 56, 0.33) 100%, #FF943E 100%)',
    gradientFullSuccess:
      'linear-gradient(180deg, #5BC188 31.92%, #6BEAA3 99.91%)',
    gradientFullPrimaryDanger:
      'linear-gradient(180deg, #4B99FF 0%, #FF8A8A 100%)',
    gradientFullPrimaryWarning:
      'linear-gradient(180deg, #4B99FF 0%, #FFB87F 100%)',
    gradientFullPrimarySuccess:
      'linear-gradient(180deg, #4B99FF 0.25%, #38E0C5 100%)',

    brandColorsPrimary: '#006EFF',
    brandColors01: '#6871B6',
    brandColors02: '#4EE0BC',
    brandColors03: '#FFE5CD',
    brandColors04: '#F8B03E',
    brandColors05: '#FED385',
    brandColors06:
      'linear-gradient(113.32deg, #BC5980 25.57%, #EE656F 101.1%, #FF6A6E 101.1%)',
    brandColors07: '#232E7B',
    brandColors08: '##FB5858',
    brandColors09: 'linear-gradient(180deg, #082054 0%, #203956 100%);',
    brandColors10: '#FF3636',
  },
};
