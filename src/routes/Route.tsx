import {
  RouteProps as ReactDOMRouterProps,
  Route as ReactDOMRouter,
  Redirect,
} from 'react-router-dom';
import { useAuth } from '../hooks/AuthContext';

interface RouteProps extends ReactDOMRouterProps {
  isPrivate: boolean;
  component: React.ComponentType;
}

const Route = ({ isPrivate, component: Component, ...props }: RouteProps) => {
  const { user } = useAuth();

  return (
    <ReactDOMRouter
      {...props}
      render={({ location }) => {
        return isPrivate === !!user ? (
          <Component />
        ) : (
          <Redirect
            to={{
              pathname: isPrivate ? '/' : '/dashboard',
              state: { from: location },
            }}
          />
        );
      }}
    />
  );
};

export default Route;
