import { Switch } from 'react-router-dom';
import Route from './Route';

import SignIn from '../pages/SignIn';
import Dashboard from '../pages/Dashboard';
import CompanyRegister from '../pages/CompanyRegister';
import StudentRegister from '../pages/StudentRegister';

const Routes = () => (
  <Switch>
    <Route path="/dashboard" component={Dashboard} isPrivate />
    <Route path="/" exact component={SignIn} isPrivate={false} />
    <Route path="/company" component={CompanyRegister} isPrivate={false} />
    <Route path="/student" component={StudentRegister} isPrivate={false} />
  </Switch>
);

export default Routes;
